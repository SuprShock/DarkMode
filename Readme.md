 Descrição:
 Esse projeto, foi desenvolvido tendo em vista a resolução do desafio Theme Switcher, proposto pela plataforma Discover da RocketSeat.
 
 Nesse projeto, uma página deve conter um botão que recebe toggle  para alternar entre o tema dark e light, salvando o resultado no LocalStorage.
 
 Layout disponivel em https://www.figma.com/file/yJ0kcX1684XPoyJnUf1K6J/DD-Theme-Switcher/duplicate.

 Desafio Disponivel em https://efficient-sloth-d85.notion.site/Desafio-Theme-Switcher-dbabdf77f70d43298df382c8e805fc13



 Feito com 💜 por SuprShock
